export class AbstractReducer {
  resetStatus = (state) => {
    for (const property in state) {
      if (property.includes('status') || property.includes('err'))
        state[property] = null;
    }
  };

  test = (state, action) => {
    state.err = 'test success';
  };
}

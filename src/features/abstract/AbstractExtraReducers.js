import { STATUS_API } from 'src/common/utils/constant';

import AxiosAdapter from 'src/common/utils/AxiosAdapter';

export class AbstractExtraReducers {
  constructor(nameSlice, keyNameCatchError) {
    this.nameSlice = nameSlice || 'defaultSlice';
    this.keyNameCatchError = keyNameCatchError || 'error';
  }

  getDetailItem = (url, type) => {
    let name = this.nameSlice;
    return AxiosAdapter.HttpGet(`${name}/getDetailItem`, url, type);
  };

  getAsyncLogic = (builder) => {
    builder
      .addCase(`${this.nameSlice}/getDetailItem/pending`, (state, action) => {
        state.statusGetDetail = STATUS_API.PENDING;
        state.err = null;
        state.itemDetail = null;
      })
      .addCase(`${this.nameSlice}/getDetailItem/rejected`, (state, action) => {
        state.statusGetDetail = STATUS_API.ERROR;
        state.err =
          action.payload?.[this.keyNameCatchError] || action.error?.message;
      })
      .addCase(`${this.nameSlice}/getDetailItem/fulfilled`, (state, action) => {
        state.statusGetDetail = STATUS_API.SUCCESS;
        state.itemDetail = action.payload;
      });
    // .addDefaultCase((state, action) => {
    //   state.err = 'sr my bad';
    // });
  };
}

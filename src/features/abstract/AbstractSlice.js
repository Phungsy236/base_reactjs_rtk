import { createSlice } from '@reduxjs/toolkit';
import { AbstractReducer } from './AbstractReducer';
import { AbstractState } from 'src/features/abstract/AbstractState';
import { AbstractExtraReducers } from 'src/features/abstract/AbstractExtraReducers';

export class AbstractSlice {
  //  stateBehaviour must extend AbstractState()
  constructor(
    nameSlice,
    keyNameCatchError,
    stateBehaviour,
    reducerBehaviour,
    extraReducersBehaviour
  ) {
    this.nameSlice = nameSlice || 'abstractSlice';
    this.keyNameCatchError = keyNameCatchError || 'error';
    this.stateBehaviour = stateBehaviour || { ...new AbstractState() };
    this.reducerBehaviour = reducerBehaviour || { ...new AbstractReducer() };
    this.extraReducers = extraReducersBehaviour || {
      ...new AbstractExtraReducers(this.nameSlice, this.keyNameCatchError)
    };
  }

  initSlice = (
    nameSlice = this.nameSlice,
    stateBehaviour = { ...this.stateBehaviour },
    reducerBehaviour = this.reducerBehaviour,
    extraReducerBehaviour = this.extraReducers.getAsyncLogic
  ) =>
    createSlice({
      name: nameSlice,
      initialState: stateBehaviour,
      reducers: reducerBehaviour,
      extraReducers: extraReducerBehaviour
    });
  // for config store only
  getReducer() {
    return this.initSlice().reducer;
  }

  getAction() {
    return this.initSlice().actions;
  }
  getState(stateName) {
    let nameSlice = this.nameSlice;
    return function (state) {
      return state[nameSlice][stateName];
    };
  }
}

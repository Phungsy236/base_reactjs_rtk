export class AbstractState {
  constructor() {
    this.listItem = null;
    this.totalItem = 0;
    this.itemDetail = null;
    this.errorGetList = null;
    this.err = null;
    this.statusGetDetail = null;
    this.statusCreateItem = null;
    this.statusUpdateItem = null;
    this.statusDeleteItem = null;
    this.statusGetAll = null;
  }
}

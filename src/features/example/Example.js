import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ExampleExtraReducer, ExampleSlice } from './ExampleSlice';
import { HTTP_GETTYPE, STATUS_API } from 'src/common/utils/constant';
const exampleSlice = new ExampleSlice();

function Example(props) {
  console.log('example render');
  const dispatch = useDispatch();
  // example call action
  // dispatch(exampleSlice.getAction().test());

  //example call async action
  const detailItem = useSelector(exampleSlice.getState('itemDetail'));
  const statusGetDetail = useSelector(exampleSlice.getState('statusGetDetail'));
  const err = useSelector(exampleSlice.getState('err'));
  let number = Math.floor(Math.random() * 20);
  useEffect(() => {
    dispatch(
      new ExampleExtraReducer().getDetailItem(
        'https://picsum.photos/id/',
        HTTP_GETTYPE.DETAIL
      )(`${number}/info`)
    );
    // eslint-disable-next-line
  }, [dispatch]);
  /*app
   * new ExampleExtraReducer().getDetailItem return a function ()=>AxiosAdapter...
   * (1) is payload
   * */
  if (err) return <div style={{ color: 'darkred' }}>{err}</div>;
  return statusGetDetail === STATUS_API.PENDING ? (
    <div>loading img ...</div>
  ) : (
    detailItem && (
      <>
        <img
          alt={'fetchimg'}
          src={detailItem.download_url}
          style={{ width: 200, height: 200 }}
        />
        <button onClick={() => dispatch(exampleSlice.getAction().test())}>
          Call alert
        </button>
      </>
    )
  );
}

export default Example;

import { AbstractSlice } from '../abstract/AbstractSlice';
import { AbstractReducer } from 'src/features/abstract/AbstractReducer';
import { AbstractState } from 'src/features/abstract/AbstractState';
import { AbstractExtraReducers } from 'src/features/abstract/AbstractExtraReducers';

// Example have all parent state and new state : message
export class ExampleInitState extends AbstractState {
  constructor() {
    super();
    this.message = '';
  }
}

// can write in other file if long
export class ExampleReducer extends AbstractReducer {
  /*    already have resetStatus() and test() of parent
       define new reducer*/
  saySomething = (state, action) => {
    state.message = action.payload;
  };
}

export class ExampleExtraReducer extends AbstractExtraReducers {
  constructor() {
    super('exampleSlice', 'error');
  }
}

export class ExampleSlice extends AbstractSlice {
  /* default constructor if example have nothing difference from parent
  constructor() {
    super('ExampleSlice', 'error', null, null, null);
  }*/
  constructor() {
    super(
      'exampleSlice',
      'error',
      new ExampleInitState(),
      new ExampleReducer(),
      new ExampleExtraReducer()
    );
  }
}

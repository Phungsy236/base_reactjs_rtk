
export function currencyFormat(num) {
  num = Number.parseInt(num);
  return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + ' VNĐ';
}
export const ArrUtil = {
  removeElmByValue: (value, arr) => {
    let index = arr.indexOf(value);
    if (index > -1) {
      arr.splice(index, 1);
    } else return false;
  },
  sortDescByKey: (value, keyName) => {
    let temp = [...value];
    return temp.sort((a, b) => {
      if (a[keyName] < b[keyName]) return 1;
      if (a[keyName] > b[keyName]) return -1;
      return 0;
    });
  },
  sortAscByKey: (value, keyName) => {
    let temp = [...value];
    return ArrUtil.sortDescByKey(temp, keyName).reverse();
  }
};



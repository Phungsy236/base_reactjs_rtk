export const TIMEOUT_DEFAULT = 4500;
export const MSG_TIMEOUT_REQUEST = 'Server phản hồi quá lâu , vui lòng thử lại';
export const HTTP_GETTYPE = {
  ALL: 0,
  ALL_PAGINATION: 1,
  DETAIL: 2
};
export const HEADER_AUTH_KEY_NAME = 'access_token';
export const STATUS_API = {
  PENDING: 0,
  SUCCESS: 1,
  ERROR: 2
};
export const SORT_TYPE = {
  DESC: 1,
  ASC: 2
};
export const PAGE_SIZE_LIST = 10;

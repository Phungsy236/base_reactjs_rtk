import { configureStore } from '@reduxjs/toolkit';
import { ExampleSlice } from '../features/example/ExampleSlice';

export const store = configureStore({
  reducer: {
    exampleSlice: new ExampleSlice().getReducer()
  }
});

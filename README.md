

### ProjectName : ```ReactJs_ReduxTookit_Base#v1```

### How to start :
1. install lib ```npm install```
2. create ```.env``` file with content :   ```REACT_APP_BACKEND_URL = ''```
3. run ```npm start```
### Project Idea : 
1. AbstractSlice is a wrapper slice with initState , crud extraReducer for basic model
    see more in ```/src/features/abstract```
2. When create new Slice ,you can extend or override abstract state , abstract reducer , abstract extraReducer
   to make your slice or create new, see example in 
    ```/src/features/example```
### Author : ```Phùng Văn Sỹ```
